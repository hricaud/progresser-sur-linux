# Pour commencer  
Je commence avec un rappel de commande dans l'environnement Linux.
La majorité des distributions Linux, fonctionne avec le **BASH** (c'est le shell qui permet d'intérargir avec le systéme, un interprétateur de commande).
le répertoire personnelle est construit dans le ``/home``

Le prompt nous situe dans notre machine:

``galak@ubuntu:~$`` 
> - ``galak`` est l'utilisateur connecté
> - ``ubuntu`` est le nom de la machine 
> - ``~`` indique que je suis dans le répertoire personnel.

**On peut savoir à tout moment ou on est en tapant la commande ``pwd`` (print workind directory=Affiche répertoire courant)**





### Option de commande

les commandes ont souvent beaucoup d'option qui changent le comportement des commandes.
Exemple :

    $ ls -l
    
> Affiche un liste longue > qui nous affiche le type de répertoire suivi des permissions, le propriétaire + le groupe, le vrai chemin d'un lien symbolique ...

    $ ls -a

> Affiche fichier caché avec un ``.``
  On peut du coup combiner ces deux options : 
  
    $ ls -al
    
> nous permet d'avoir une liste longue des fichiers cachés avec les détails.



#### Les commandes Indispensables :

- ``Man`` : Manuel linux.
- ``cd`` : Se déplacer dans linux.
- ``ls`` : lister le contenu du répertoire courant. 
- ``mkdir/rmdir``: Créer/supprimer un réperoire. 
- ``clear`` : nettoie la fenêtre du terminal.
- ``pwd `` : affiche le repertoire dans lequel on est
